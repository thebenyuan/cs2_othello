#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class Player {
private:
	Side side;

public:
        Board b;

    Player(Side side);
    ~Player();
    
    double Heuristic(Side side, Board *testBoard, int x, int y);
    Move *Minimax(Side side);
    Move *doMove(Move *opponentsMove, int msLeft);
    Move *doMove1(Move *opponentsMove, int msLeft);
    Move *doMove0(Move *opponentsMove, int msLeft);
    Move *doMove2(Move *opponentsMove, int msLeft);



    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
