#include "player.h"
// add comment for points, yay: Grace Park
// Add comment for points: David Cheng


/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */

    // Board *b;
    this->side = side;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
    // delete b;
}

/*
* @brief calculates the heuristic value of a given move.
*
* @param side: which player has the turn
* @param testBoard: the current board we are looking at
*               (in the case of minimax, the board from the level above)
* @param m: the move to be made
*/
double Player::Heuristic(Side side, Board *testBoard, int x, int y)
{
    // this is the variable used for the heuristic value
    double diff;

    // depending on what side you are, find simple hueristic value
    if (side == BLACK)
    {
        diff = testBoard->countBlack() - testBoard->countWhite();
    }
    else
    {
        diff = testBoard->countWhite() - testBoard->countBlack();
    }

    // if the move is either a edge, corner, or a position
    // near the edge, modify the heuristic value accordingly

    if (x == 0 || x == 7)
    {
        diff *= 1.75;
        if (y == 1 || y == 6)
        {
            diff *= -1.75;
        }
    }

    if (y == 0 || y == 7)
    {
        diff *= 1.75;
        if (x == 1 || x == 6)
        {
            diff *= -1.75;
        }
    }

    if ((x == 1 || x == 6) && (y == 1 || y == 6))
    {
        diff *= -4;
    }


    // return final hueristic value
    return diff;
}

/*
* @brief: a minimax algorithm to play othello
*
* @params: side who is supposed to make the move
*/
Move *Player::Minimax(Side side)
{
    // give the opponent a side
    Side other = (side == BLACK) ? WHITE : BLACK;

    Move * currBest;

    // we want to keep track of the max & possible moves now
    int max = -10000;
    std::vector<Move> moves = b.getValidMoves(side);

    // for each possible move
    for (unsigned int i = 0; i < moves.size(); i++)
    {
        // make a copy of the board
        Board *copy = b.copy();

        // make the move
        int x1 = moves[i].getX();
        int y1 = moves[i].getY();
        Move *m = new Move(x1,y1);
        
        copy -> doMove(m, side);

        // find possible moves of the opponent for the
        // move that we just made
        std::vector<Move> opp_moves = copy->getValidMoves(other);
        // for each of the opponent's moves, calculate the heuristic
        for (unsigned int j = 0; j < opp_moves.size(); j++)
        {
            // by making a copy of the copy board
            Board *secondCopy = copy->copy();

            // and processingthe opponents move
            int x2 = opp_moves[j].getX();
            int y2 = opp_moves[j].getY();
            Move *l = new Move(x2,y2);
            secondCopy -> doMove(l, other);

                // calculate heuristic
                int h = Heuristic(side, secondCopy, x2, y2);
                // update whenever we find a better choice
                if (h > max)
                {
                    max = h;
                    currBest = m;
                }
        }
    }
    //return the ultimate best
    return currBest;
} 

/*
* @brief plays othello by using the minimax algorithm (if testing minimax)
*   else, it uses only the heuristic value.
*
*/
Move *Player::doMove(Move *opponentsMove, int msLeft)
{
    // process the other players move
    Side other = (side == BLACK) ? WHITE : BLACK;
    b.doMove(opponentsMove, other);

    // keep track of the best move & max heuristic value
    Move * best;

    if (testingMinimax)
    {
        // call minimad to choose what move we want to make
        best = Minimax(side);
        // process our move
        b.doMove(best, side);
    }

    else
    {
    int max = -100;
    int diff;

    // find all the valid moves available for us
    std::vector<Move> moves = b.getValidMoves(side);

    // if there arent any, give up.
    if (moves.size() == 0)
    {
        return NULL;   
    }

    // if there are valid moves, find the best one
    for (unsigned int i = 0; i < moves.size(); ++i)
    {
        // copy the current board
        Board * old = b.copy();

        // create a move * for each valid move
        int x = moves[i].getX();
        int y = moves[i].getY();
        Move *m = new Move(x, y);

        // process the move on the copy board
        old -> doMove(m, side);

        diff = Heuristic(side, old, x, y);
        // for each iteration, find the highest heuristic value
        if (diff > max)
        {
            max = diff;
            // and update the best move such that we get
            // the best score in this move 
            best = m;
        }
    }
        // process our move
        b.doMove(best, side);
    }
    
    return best;
}

/*
 * Play Othello with the heuristic values only
 */
Move *Player::doMove1(Move *opponentsMove, int msLeft) {

    // process the opponent's move first
    Side other = (side == BLACK) ? WHITE : BLACK;
    b.doMove(opponentsMove, other);

    // keep track of the best move & max heuristic value
    Move * currBest;
    int max = -100;
    int diff;

    // find all the valid moves available for us
    std::vector<Move> moves = b.getValidMoves(side);

    // if there arent any, give up.
    if (moves.size() == 0)
    {
        return NULL;   
    }

    // if there are valid moves, find the best one
    for (unsigned int i = 0; i < moves.size(); ++i)
    {
        // copy the current board
        Board * old = b.copy();

        // create a move * for each valid move
        int x = moves[i].getX();
        int y = moves[i].getY();
        Move *m = new Move(x, y);

        // process the move on the copy board
        old->doMove(m, side);

        diff = Heuristic(side, old, x, y);
        // for each iteration, find the highest heuristic value
        if (diff > max)
        {
            max = diff;
            // and update the best move such that we get
            // the best score in this move 
            currBest = m;
        }
    }

    // process the 'bestmove' that we found
    b.doMove(currBest, side);
    return currBest;

}


/*
 * This is the simple player algorithm where we choose random moves
 *
 */
Move *Player::doMove0(Move *opponentsMove, int msLeft) {

    // process the opponent's moves
    Side other = (side == BLACK) ? WHITE : BLACK;
    b.doMove(opponentsMove, other);

    //find out what moves we can make
    std::vector<Move> moves = b.getValidMoves(side);

    // make the moves, given that we have valid moves
    if (moves.size() != 0)
    {
        int x = moves[0].getX();
        int y = moves[0].getY();
        Move *m = new Move(x, y);
        b.doMove(m, side);
        return m;
    }
    return NULL;
}
